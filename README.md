# Zo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.0.

## Notes

Check why Leaflet doesn't allow to extend itself through angular-cli (look here https://github.com/consbio/Leaflet.ZoomBox/issues/15)

Adjust zoom level to a geocoder's response values (boundedBy)

Probably should avoid using z-index

Probably should add more interfacing to a service
