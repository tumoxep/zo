import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpResponse,
  HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { Observable } from 'rxjs/Observable';

export interface Geocoder {
  response: any;
}

@Injectable()
export class GeocoderService {
  yaGeocoderAPIUrl = 'https://geocode-maps.yandex.ru/1.x/';

  constructor(private http: HttpClient) { }

  getLatLng(searchqr: string) {
    searchqr = searchqr.trim();
    const options = {
      params: new HttpParams().set('format', 'json')
                              .set('geocode', searchqr)
    };
    return this.http.get<Geocoder>(this.yaGeocoderAPIUrl, options)
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error(error.error.message);
    } else {
      console.error(error.status);
    }
    return new ErrorObservable('Request error');
  };
}
