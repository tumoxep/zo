import { Component } from '@angular/core';
import { GeocoderService } from './geocoder/geocoder.service';
import { Map, icon, latLng, marker, tileLayer } from 'leaflet';

declare var L;
declare var YandexOverlay;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [ GeocoderService ],
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  latitude: string;
  longitude: string;
  options = {
    zoom: 7,
    zoomControl: false,
    center: latLng([55, 37])
  };
  layers = [
    new YandexOverlay(),
  ];
  marker = marker([42, 42], {
     icon: icon({
        iconSize: [ 25, 41 ],
        iconAnchor: [ 13, 41 ],
        iconUrl: 'assets/marker-icon.png',
        shadowUrl: 'assets/marker-shadow.png'
     })
  })
  mapobj: Map;

  constructor(private geocoderService: GeocoderService) {}

  findLatLng(searchquery: string) {
    searchquery = searchquery.trim();
    if (!searchquery) { return; }
    this.geocoderService.getLatLng(searchquery)
      .subscribe(
        data => {
          const point = data.response
                            .GeoObjectCollection
                            .featureMember[0]
                            .GeoObject
                            .Point
                            .pos;
          [this.longitude, this.latitude] = point.split(' ');
          this.marker.setLatLng(latLng([parseFloat(this.latitude), parseFloat(this.longitude)]));
          this.mapobj.setView(latLng([parseFloat(this.latitude), parseFloat(this.longitude)]), this.options.zoom);
          if (this.layers.length === 1) {
            this.layers.push(this.marker);
          }
        },
        error => console.error(error)
      );
  }

  onMapReady(map: Map) {
    this.mapobj = map;
  }
}
